#/bin/bash
for file in `(ls *.HEIC)`
do 
    echo $file 
    JPG_FILE="${file%.HEIC}.JPG"
    sips --setProperty format jpeg "$file" --out $JPG_FILE
    if [ $? -eq 0 ] 
    then
        # Keep the same date as original file
        touch -r $file $JPG_FILE
        # Remove HEIC photo 
        rm $file
    fi
done 

